# Coding conventions

Use common sense. All these rules can be broken, but for a good reason only.

#### JVM languages
 * Prefered: Java 14
 * Allowed: Java 8

#### UI
 * Allowed: JavaFX

#### Build tool
 * Preferred: Gradle
 * Allowed: Maven

#### Testing
 * Preferred: Spock/Groovy
 * Allowed: JUnit4, JUnit5

At least happy-path functionality should be covered by unit tests.

#### Logging
 * Preffered: Logback.

Logging should be verbose and describe all major operations. Logs are the only information you'll have from the client workstation when an issue occurs. More logging is always better than less logging. 

#### Error handling

 * All errors have to be logged and a user should have a possibility to see them. There are two ways to do it, show error message dialog, or print error message in the logging TextArea (see further). These two approaches can be combined.
 * If you get an unexpected exception, stop execution cycle, only if project requirements don't tell otherwise.

#### Settings
 * Preferred: Yaml.
 * Allowed: Java properties

Settings file should be ignored by git, and the repository has to have a sample of settings with a different name, for instance, "settings-sample.yml".
It helps to avoid a developer's specific info saving in the repository.

All sensitive information as passwords and secret keys should be encrypted.

#### Packaging
 * Project should be able to create the fat JAR, for instance by a [shadow Gradle plugin](https://github.com/johnrengelman/shadow) plugin
 * Follow [common versioning convention](https://semver.org): MAJOR, MINOR, PATCH/FIX. Increment respective part before each task or fix.

### Coding style:
 * Line width: <= 120
 * All blocks should be wrapped by curly braces, even one liners

### Code quality
 * No warnings at compilation or at runtime
 * No unused imports
 * No unused variables
 * No commented or unused code
 * IDE-specific, compiled binaries, and temporary files should be ignored in a repository

## Branding

 1. Packages name should be started with "com.m8fintech.[app name]"