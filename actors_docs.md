#### Rules

* Do not block. If it really required, make sure actor is working on special dispatcher. But it is recommended to not block inside actor. Use completable futures, and run them on any desired thread pool.

* Messages must be immutable. Lombok `@Value` helps with that.

* Only non modifiable collections are allowed to be send to other actor. If it is not, new collection should be created and sent.

* Collections in messages must contain only immutable objects.

#### Actor template, also that is the code style of default actor.

```
public class ExampleActor extends AbstractBehavior<ExampleActor.Command> {

	public static Behavior<Command> create() {
		return Behaviors.logMessages(Behaviors.setup(ctx -> new ExampleActor(ctx)));
	}

	private ExampleActor(ActorContext<Command> ctx) {
		super(ctx);
	}

	@Override
	public Receive<Command> createReceive() {
		return newReceiveBuilder()
				.build();
	}

	public interface Command {}
}
```

#### Additional templates

##### Abstract behavior replacement

```
import akka.actor.typed.ActorRef;
import akka.actor.typed.eventstream.EventStream;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.japi.function.Function;
import akka.japi.function.Function2;
import org.slf4j.Logger;

import java.util.concurrent.CompletionStage;

public abstract class TypedActor<T> extends AbstractBehavior<T> {
    public TypedActor(ActorContext<T> context) {
        super(context);
    }

    public ActorRef<EventStream.Command> getEventStream() {
        return getContext().getSystem().eventStream();
    }

    public Logger getLog() {
        return getContext().getLog();
    }

    public <R> void pipeToSelf(CompletionStage<R> future, Function2<R, Throwable, T> applyToResult) {
        getContext().pipeToSelf(future, applyToResult);
    }
    
    public void tellToSelf(T command) {
        getContext().getSelf().tell(command);
    }

    public <C> void subscribeToEventStream(Class<C> clazz, ActorRef<C> actorRef) {
        getEventStream().tell(new EventStream.Subscribe<>(clazz, actorRef));
    }

    public <R> ActorRef<R> messageAdapter(Class<R> clazz, Function<R, T> mapper) {
        return getContext().messageAdapter(clazz, mapper);
    }
}

```

##### AskPattern helper

```
import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.RecipientRef;
import akka.actor.typed.javadsl.AskPattern;
import akka.pattern.StatusReply;
import com.m8fintech.gridtradingbot.actor.Guardian;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.function.Function;

/**
 * Helps to simplify {@link AskPattern} usage.
 */
@RequiredArgsConstructor
public class AskPatternHelper {

    private final ActorSystem<Guardian.Command> actorSystem;

    public <Req, Resp> Mono<Resp> ask(RecipientRef<? super Req> actorRef, Function<ActorRef<Resp>, Req> reqFactory) {
        return ask(actorRef, reqFactory, Constants.ActorReqTimeouts.LONG);
    }

    public <Req, Resp> Mono<Resp> ask(RecipientRef<? super Req> actorRef, Function<ActorRef<Resp>, Req> reqFactory,
            Duration timeout) {

        return Mono.fromCompletionStage(() -> AskPattern.ask(
                actorRef,
                reqFactory::apply,
                timeout,
                actorSystem.scheduler()
        ));
    }

    public <Req, Resp> Mono<Resp> askWithStatus(RecipientRef<? super Req> actorRef,
            Function<ActorRef<StatusReply<Resp>>, Req> reqFactory) {

        return askWithStatus(actorRef, reqFactory, Constants.ActorReqTimeouts.LONG);
    }

    public <Req, Resp> Mono<Resp> askWithStatus(RecipientRef<? super Req> actorRef,
            Function<ActorRef<StatusReply<Resp>>, Req> reqFactory, Duration timeout) {

        return Mono.fromCompletionStage(() -> AskPattern.askWithStatus(
                actorRef,
                reqFactory::apply,
                timeout,
                actorSystem.scheduler()
        ));
    }
}
```
##### Utils class
```
import akka.actor.typed.ActorTags;
import reactor.util.function.Tuple2;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Thead safe.
 */
public final class ActorUtils {
    private static final AtomicInteger ID = new AtomicInteger();

    /**
     * Helps with creation of unique actors name.
     */
    public static String createUniqueName(Class clazz) {

        return clazz.getSimpleName() + "-" + ID.incrementAndGet();
    }

    /**
     * Helper method to generate actor tags from array of tuples(key-value pair).
     */
    public static ActorTags prepareTags(Tuple2... tuples) {
        return ActorTags.create(Stream.of(tuples)
                .map(tuple -> tuple.getT1().toString() + "=" + tuple.getT2().toString())
                .collect(Collectors.toSet()));
    }

    private ActorUtils() {
    }
}

```
#### Messaging templates

* Request - Response
```
@Value
public static class ExampleRequest implements Command {
    private final String someData;
    private final ActorRef<ExampleResponse> replyTo;
}

@Value
public static class ExampleResponse {
    private final String someData;
}
```
* Request - Response with possible error
```
@Value
public static class ExampleRequest implements Command {
    private final String someData;
    private final ActorRef<StatusReply<ExampleResponse>> replyTo;
}

@Value
public static class ExampleResponse {
    private final String someData;
}

// usage example - sending response

replyTo.tell(StatusReply.error(new Exception())); //error case
replyTo.tell(StatusReply.success(new ExampleResponse(""))); //success case

// sending request
// regular tell with adapter (example in other docs section.)
// using ask

getContext.askWithStatus(...) // available from akka 2.6.7
AskPattern.askWithStatus(...) // available from akka 2.6.7
```
* Request without response data, only with complete confirmation and possible error
```
@Value
public static class ExampleRequest implements Command {
    private final String someData;
    private final ActorRef<StatusReply<Done>> replyTo;
}

// usage example - sending response

replyTo.tell(StatusReply.error(new Exception())); //error case
replyTo.tell(StatusReply.ack()); //success case

// sending request
// regular tell with adapter (example in other docs section.)
// using ask

getContext.askWithStatus(...) // available from akka 2.6.7
AskPattern.askWithStatus(...) // available from akka 2.6.7

```
* Command with data without response 
```
@Value
public static class ExampleCommandWithData implements Command {
    private final String someData;
}
```
* Command (no response, no data)
```
@ToString
public enum ExampleCommand implements Command {
    INSTANCE
}
```
* Request - response example (without ask method using regular tell). 
As actor cannot receive message other then its api. Message adapter is used.
When `SendingActor` starts, it sends message to `ReceivingActor`, and then receives response.
```
public class SendingActor extends AbstractBehavior<SendingActor.Command> {

    public static Behavior<Command> create(ActorRef<ReceivingActor.Command> receivingActor) {
        return Behaviors.logMessages(Behaviors.setup(ctx -> new SendingActor(ctx, receivingActor)));
    }

    private SendingActor(ActorContext<Command> ctx, ActorRef<ReceivingActor.Command> receivingActor) {
        super(ctx);

        //message adapter can be saved and reused.
        val messageAdapter = getContext().messageAdapter(ReceivingActor.Pong.class, it -> new WrappedPong(it));
        receivingActor.tell(new ReceivingActor.Ping(messageAdapter));
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(WrappedPong.class, it -> onPongResponse(it.response))
                .build();
    }

    public Behavior<Command> onPongResponse(ReceivingActor.Pong response) {
        //do something

        return Behaviors.same();
    }

    @Value
    private static class WrappedPong implements Command { //private, so no one can send that message to that actor.
        private final ReceivingActor.Pong response;
    }

    public interface Command {}
}
```
```
public class ReceivingActor extends AbstractBehavior<ReceivingActor.Command> {

    public static Behavior<Command> create() {
        return Behaviors.logMessages(Behaviors.setup(ctx -> new ReceivingActor(ctx)));
    }

    private ReceivingActor(ActorContext<Command> ctx) {
        super(ctx);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(Ping.class, it -> onPingRequest(it))
                .build();
    }

    public Behavior<Command> onPingRequest(Ping request) {
        request.replyTo.tell(Pong.INSTANCE);

        return Behaviors.same();
    }

    @Value
    public static class Ping implements Command {
        private final ActorRef<Pong> replyTo;
    }

    @ToString
    public enum Pong {
        INSTANCE
    }

    public interface Command {}
}
```
#### Logging tips

* Good approach to log all messages actors receives. Use `Behaviors.withLogging` and wrap original behaviour within it. Example `Behaviors.withLogging(Behaviors.setup(ctx -> {})`
	
* Make sure that all messages implements to string method, even enums.
	
* All logger appenders must be async. In non spring projects appenders shutdown must be handled manualy. In spring projects - spring will do it automatiacally.
	
* `Logback` example
```
	<configuration scan="true" scanPeriod="10 seconds">
	<appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
			<level>DEBUG</level>
		</filter>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} | %-5level | %thread | %logger{36}: %msg | MDC: %mdc%n</pattern>
		</encoder>
	</appender>

	<appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
			<level>DEBUG</level>
		</filter>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} | %-5level | %thread | %logger{0}: %msg | MDC: %mdc%n</pattern>
		</encoder>

		<file>logs/prdx_market_maker.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
			<fileNamePattern>logs/prdx_market_maker-%d{yyyy-MM-dd}_%i.log.zip</fileNamePattern>
			<maxFileSize>200MB</maxFileSize>
			<maxHistory>50</maxHistory>
			<totalSizeCap>40GB</totalSizeCap>
		</rollingPolicy>
	</appender>

	<appender name="TRACE-FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
			<level>TRACE</level>
		</filter>
		<encoder>
			<pattern>%d{HH:mm:ss.SSS} | %-5level | %thread | %logger{0}: %msg | MDC: %mdc%n</pattern>
		</encoder>

		<file>logs/prdx_market_maker_trace.log</file>
		<rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
			<fileNamePattern>logs/prdx_market_maker_trace-%d{yyyy-MM-dd}_%i.log.zip</fileNamePattern>
			<maxFileSize>200MB</maxFileSize>
			<maxHistory>50</maxHistory>
			<totalSizeCap>40GB</totalSizeCap>
		</rollingPolicy>
	</appender>

	<appender name="ASYNC-FILE" class="ch.qos.logback.classic.AsyncAppender">
		<neverBlock>true</neverBlock>
		<queueSize>1000000</queueSize>
		<discardingThreshold>0</discardingThreshold>
		<appender-ref ref="FILE"/>
	</appender>

	<appender name="ASYNC-TRACE-FILE" class="ch.qos.logback.classic.AsyncAppender">
		<neverBlock>true</neverBlock>
		<queueSize>1000000</queueSize>
		<discardingThreshold>0</discardingThreshold>
		<appender-ref ref="TRACE-FILE"/>
	</appender>

	<appender name="ASYNC-STDOUT" class="ch.qos.logback.classic.AsyncAppender">
		<neverBlock>true</neverBlock>
		<queueSize>1000000</queueSize>
		<discardingThreshold>0</discardingThreshold>
		<appender-ref ref="STDOUT"/>
	</appender>

	<root level="TRACE">
		<appender-ref ref="ASYNC-STDOUT"/>
		<appender-ref ref="ASYNC-FILE"/>
		<appender-ref ref="ASYNC-TRACE-FILE"/>
	</root>

	<logger name="javax.management" level="INFO"/>
	<logger name="org.springframework" level="WARN"/>
	<logger name="org.springframework.boot.autoconfigure" level="WARN"/>
	<logger name="org.hibernate" level="INFO"/>
	<logger name="com.zaxxer.hikari" level="INFO"/>
	<logger name="reactor.netty" level="INFO"/>
	<logger name="springfox.documentation" level="INFO"/>
	<logger name="sun.rmi" level="INFO"/>
	<logger name="java.io.serialization" level="INFO"/>

</configuration>
```
#### Actor naming

#### Spawning rules
* By default if uncaught exception in actor happens, the actor will stop.
To override that, wrap actor behaviors with supervised on. Example:
```
getContext.spawnAnonymous(
    Behaviors.supervise(TestActor.create())
                .onFailure(SupervisorStrategy.resume())
)
```
	
* Make sure when spawning multiple actors, that unique name for actor is set.

#### Interaction from outside the system
* Use `AskPattern.ask(/* params */)`.

* To comfortably interact with an actor from outside of the system (in ask manner). Create a wrapper class for this actor, which will hide `AskPattern` inside it, and will return `CompletableFuture`.
Example: In example also `Mono` is used. That is not mandatory.
```
public class SymbolManagerWrapper {
	private final ActorRef<SymbolsManagerActor.Command> symbolManagerActor;
	private final ActorSystem<Guardian.Command> actorSystem;

	public SymbolManagerWrapper(ActorSystem<Guardian.Command> actorSystem,
			ActorRef<SymbolsManagerActor.Command> symbolManagerActor) {
		this.symbolManagerActor = symbolManagerActor;
		this.actorSystem = actorSystem;
	}

	public Mono<SymbolsManagerActor.GetAllSymbolsResponse> getAllSymbols() {

		CompletionStage<SymbolsManagerActor.GetAllSymbolsResponse> future = AskPattern.ask(
				symbolManagerActor,
				SymbolsManagerActor.GetAllSymbols::new,
				ACTOR_REQUEST_TIMEOUT,
				actorSystem.scheduler()
		);

		return Mono.fromCompletionStage(future);
	}
}
```
#### Unexpected Tips and tricks. Or tricky things.
* `getContext().getLog()` logger in actor is not allowed to save or cache, as when `getLog()` is called - mdc context is setup.

* If parent actor is watching child actor. Parent must unwatch before stopping child. Also parent should unwatch on own stop and restart signals, otherwise child terminated signal will be received.
	
* By default. Deadletter will be stopped from logging for 5 minutes if alot of deadletters appears (5 at short period of time, arround several ms).
Add `akka.log-dead-letters = on` to application.conf to log all deadletters.
 
* `context.unwatch` do not unwatch custom terminated message registered using `context.watchWith`.
	
* Building shadow jar. When launching fat jar build by shadow jar you may incounter reference.conf problem.
Thats happing bacause of how akka configuration works, inside akka dependencies there are multiple conf files, when it is single fat jar there should single config file. Which must be merged.
That can be done like in example below. On spring using bootJar task, havent been encountered such problem.
```
import com.github.jengelman.gradle.plugins.shadow.transformers.AppendingTransformer
	
plugins {
	id 'java'
	id "com.github.johnrengelman.shadow" version "5.0.0"
}
	
shadowJar {
	transform(AppendingTransformer) {
		resource = 'reference.conf'
	}
	with jar
}
```
* Using `getContext.ask()` - possible timeout "corner case" must be handled. Example situation below.
We provide `REQUEST_TIMEOUT` constant - timeout duration config. After symbolsManager or after `REQUEST_TIMEOUT` reached - `new GetSymbolActorMappedResponse(request, symbolResponse, error)` message will be sent to self.
If timeout happens, actor will not listen to response. In real world that is possible that target actor, will respond after timeout. And as we are no more interested in answer, it will go to dead letter.
If you want to handle that response even after timeout reached. Setup message adapter when actor starts to be able to receive `SymbolsManagerActor.GetSymbolActor.Response.class` all the time. 
That can be done with:
`getContext().messageAdapter(SymbolsManagerActor.GetSymbolActor.Response.class, msg -> new WrappedMessage(msg));`
Then your actor will always receive such type message, and now are able to handle messages received after timeout.
```
getContext().ask(SymbolsManagerActor.GetSymbolActor.Response.class,
            symbolsManager,
            REQUEST_TIMEOUT,
            replyTo -> new SymbolsManagerActor.GetSymbolActor(internalSymbolName, replyTo),
            (symbolResponse, error) -> new GetSymbolActorMappedResponse(request, symbolResponse, error));
```
* Event stream: by unsubscribing from one channel you will unsubscribe from all channels. Even when you are providing exact adapter. That happens because all adapters in actor have the same ActorRef
#### Good Tips and Tricks.
	
* Official documentation is good enough to get started. https://doc.akka.io/docs/akka/current/index.html
	
* Default configuration https://doc.akka.io/docs/akka/current/general/configuration-reference.html
	