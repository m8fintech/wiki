**Based on Google C++ Style Guide revision 3.274**

### Header Files ###

In general, every .cpp file should have an associated .h file. There are some common exceptions, such as unittests and small .cpp files containing just a main() function.
Correct use of header files can make a huge difference to the readability, size and performance of your code.The following rules will guide you through the various pitfalls of using header files.

### The #define Guard ###

All header files should have #define guards or equivalent to prevent multiple inclusion.

### Forward Declarations ###

You may forward declare ordinary classes in order to avoid unnecessary #includes.

### Inline Functions ###

Define functions inline only when they are small, say, 10 lines or less.

### The -inl.h Files ###
You may use file names with a -inl.h suffix to define complex inline functions when needed.

### Function Parameter Ordering ###
When defining a function, parameter order is: inputs, then outputs.

### Names and Order of Includes ###

Use standard order for readability and to avoid hidden dependencies: C library, C++ library, other libraries' .h, your project's .h.

### Namespaces ###

Unnamed namespaces in .cpp files are encouraged. With named namespaces, choose the name based on the project, and possibly its path. Do not use a using-directive. Do not use inline namespaces.

### Nested Classes ###
Although you may use public nested classes when they are part of an interface, consider a namespace to keep declarations out of the global scope.

### Nonmember, Static Member, and Global Functions ###

Prefer nonmember functions within a namespace or static member functions to global functions; use completely global functions rarely.

### Local Variables ###
Place a function's variables in the narrowest scope possible, and initialize variables in the declaration.

### Static and Global Variables ###

Static or global variables of non-const type are forbidden: they cause hard-to-find bugs due to indeterminate order of construction and destruction. However, such variables are allowed if they are constexpr: they have no dynamic initialization or destruction.

### Classes ###

Classes are the fundamental unit of code in C++. Naturally, we use them extensively. This section lists the main dos and don'ts you should follow when writing a class.

### Doing Work in Constructors ###

Avoid doing complex initialization in constructors (in particular, initialization that can fail or that requires virtual method calls).

### Initialization ###

If your class defines member variables, you must provide an in-class initializer for every member variable or write a constructor (which can be a default constructor). If you do not declare any constructors yourself then the compiler will generate a default constructor for you, which may leave some fields uninitialized or initialized to inappropriate values.

### Explicit Constructors ###

Use the C++ keyword explicit for constructors with one argument.

### Copy Constructors ###

Provide a copy constructor and assignment operator only when necessary. Otherwise, disable them.

### Delegating and inheriting constructors ###

Use delegating and inheriting constructors when they reduce code duplication.

### Structs vs. Classes ###

Use a struct only for passive objects that carry data; everything else is a class.

### Inheritance ###

Composition is often more appropriate than inheritance. When using inheritance, make it public.

### Multiple Inheritance ###

Only very rarely is multiple implementation inheritance actually useful. We allow multiple inheritance only when at most one of the base classes has an implementation; all other base classes must be pure interface classes tagged with theInterface suffix.

### Operator Overloading ###

Do not overload operators except in rare, special circumstances.

### Access Control ###

Make data members private, and provide access to them through accessor functions as needed (for technical reasons, we allow data members of a test fixture class to be protected when using Google Test). Typically a variable would be called m_foo and the accessor function foo(). You may also want a mutator function setFoo(). Exception: static const data members (typically called FOO) need not be private.

### Declaration Order ###

Use the specified order of declarations within a class: public: before private:, methods before data members (variables), etc.

### Write Short Functions ###

Prefer small and focused functions.

### Ownership and Smart Pointers ###

Prefer to have single, fixed owners for dynamically allocated objects. Prefer to transfer ownership with smart pointers.

### Reference Arguments ###

All parameters passed by reference must be labeled const.

### Rvalue references ###

Do not use rvalue references, std::forward, std::move_iterator, or std::move_if_noexcept. Use the single-argument form of std::move only with non-copyable arguments.

### Function Overloading ###

Use overloaded functions (including constructors) only if a reader looking at a call site can get a good idea of what is happening without having to first figure out exactly which overload is being called.

### Variable-Length Arrays and alloca() ###

We do not allow variable-length arrays or alloca().

### Friends ###

We allow use of friend classes and functions, within reason (for instance refactoring) and as temporal solution only.

### Run-Time Type Information (RTTI) ###

Avoid using Run Time Type Information (RTTI), except debugging information getting.

### Casting ###

Use C++ casts like static_cast<>(). Do not use other cast formats like int y = (int)x; or int y = int(x);.

### Preincrement and Predecrement ###

Use prefix form (++i) of the increment and decrement operators with iterators and other template objects.

### Use of const ###

Use const whenever it makes sense. With C++11, constexpr is a better choice for some uses of const.

### Use of constexpr ###

In C++11, use constexpr to define true constants or to ensure constant initialization.

### Integer Types ###

Of the built-in C++ integer types, the only one used is int. If a program needs a variable of a different size, use a precise-width integer type from <stdint.h>, such as int16_t. If your variable represents a value that could ever be greater than or equal to 2^31 (2GiB), use a 64-bit type such as int64_t. Keep in mind that even if your value won't ever be too large for an int, it may be used in intermediate calculations which may require a larger type. When in doubt, choose a larger type.

### 64-bit Portability ###

Code should be 64-bit and 32-bit friendly. Bear in mind problems of printing, comparisons, and structure alignment.

### Preprocessor Macros ###

Be very cautious with macros. Prefer inline functions, enums, and const variables to macros.

### 0 and nullptr/NULL ###

Use 0 for integers, 0.0 for reals, nullptr for pointers, and '\0' for chars.

### sizeof ###

Prefer sizeof(varname) to sizeof(type).

### auto ###

Use auto to avoid type names that are just clutter. Continue to use manifest type declarations when it helps readability, and never use auto for anything but local variables.

### Brace Initialization ###

You may use brace initialization.

### Boost ###
Use Boost minimally as possible. Use only necessary libraries from the Boost library collection. Always prefer standard library.

### C++ >= 17 ###
Use libraries and language extensions from C++17 or newer when appropriate. Consider portability to other environments before using C++17 features in your project.

### Naming ###

The most important consistency rules are those that govern naming. The style of a name immediately informs us what sort of thing the named entity is: a type, a variable, a function, a constant, a macro, etc., without requiring us to search for the declaration of that entity. The pattern-matching engine in our brains relies a great deal on these naming rules. Naming rules are pretty arbitrary, but we feel that consistency is more important than individual preferences in this area, so regardless of whether you find them sensible or not, the rules are the rules.

### General Naming Rules ###

Function names, variable names, and filenames should be descriptive; eschew abbreviation.

### File Names ###

Filenames should be all lowercase and can include underscores (_) or dashes (-). Follow the convention that your project uses. If there is no consistent local pattern to follow, prefer "_".

### Variable Names ###

Variable names are all lowercase, with underscores between words. Class member variables have “m_” prefix. For instance: my_exciting_local_variable, m_my_exciting_member_variable.

### Constant Names ###

Use upper case: DAYS_IN_A_WEEK.

### Function Names ###

Regular functions have mixed case; starts with lower case; accessors and mutators match the name of the variable: myExcitingFunction(), myExcitingMethod(), setMyVariable(), myVariable().

### Namespace Names ###

Namespace names are all lower-case, and based on project names and possibly their directory structure: google_awesome_project.

### Enumerator Names ###

Enumerators should be named either like constants or like macros: ENUM_NAME.

### Macro Names ###

You're not really going to define a macro, are you? If you do, they're like this: MY_MACRO_THAT_SCARES_SMALL_CHILDREN.

### Exceptions to Naming Rules ###

If you are naming something that is analogous to an existing C or C++ entity then you can follow the existing naming convention scheme.

### Comments ###

Though a pain to write, comments are absolutely vital to keeping our code readable. The following rules describe what you should comment and where. But remember: while comments are very important, the best code is self-documenting. Giving sensible names to types and variables is much better than using obscure names that you must then explain through comments.When writing your comments, write for your audience: the next contributor who will need to understand your code. Be generous — the next one may be you!

### Comment Style ###

Use either the // or /* */ syntax, as long as you are consistent.

### Function Comments ###

Declaration comments describe use of the function; comments at the definition of a function describe operation.

### Variable Comments ###

In general the actual name of the variable should be descriptive enough to give a good idea of what the variable is used for. In certain cases, more comments are required.

### Implementation Comments ###

In your implementation you should have comments in tricky, non-obvious, interesting, or important parts of your code.

### Punctuation, Spelling and Grammar ###

Pay attention to punctuation, spelling, and grammar; it is easier to read well-written comments than badly written ones.

### TODO Comments ###

Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect.

### Deprecation Comments ###

Mark deprecated interface points with DEPRECATED comments

### Doxygen comments ###

All public function should to be described with Doxygen Javadoc style. Also in other part Doxygen is highly recommended. You may use JAVADOC_AUTOBRIEF option (no @brief tag). You may ignore this rule, if function name and parameters are absolutely clear, and no complicated logic exists.

```
#!c++
/**
 Changes speed of engine

 @param rpm Speed in rpm
 @returns true – on success, false – on error
 */
bool setSpeed(uint32_t rpm);

```

### Compilation ###
Your code must have no warning. Use -Wall or equivalent.

### Formatting ###

Coding style and formatting are pretty arbitrary, but a project is much easier to follow if everyone uses the same style. Individuals may not agree with every aspect of the formatting rules, and some of the rules may take some getting used to, but it is important that all project contributors follow the style rules so that they can all read and understand everyone's code easily.

### Curly braces ###

For namespaces, classes and functions curly braces have to located on the next line. In other cases - braces have to be on their own line.

```
#!c++
namespace test
{
    void request()
    {
        if (variable) {
            ....
        }
        else {
            ....
}
    }
 
    class MegaClass
    {
        ...
    }
}
```

### Line Length ###

Each line of text in your code should be at most 120 characters long.

### Non-ASCII Characters ###

Non-ASCII characters should be rare, and must use UTF-8 formatting.

### Spaces vs. Tabs ###

Use only spaces, and indent 4 spaces at a time.

### Declarations and Definitions ###

Return type on the same line as function name, parameters on the same line if they fit.

### Function Calls ###

On one line if it fits; otherwise, wrap arguments at the parenthesis.

### Braced Initializer Lists ###

Format a braced list exactly like you would format a function call in its place.

### Conditionals ###

Prefer no spaces inside parentheses. The else keyword belongs on a new line.

### Loops and Switch Statements ###

Switch statements may use braces for blocks. Annotate non-trivial fall-through between cases. Empty loop bodies should use {} or continue.

### Pointer and Reference Expressions ###

No spaces around period or arrow. Pointer operators do not have trailing spaces.

### Boolean Expressions ###

When you have a boolean expression that is longer than the standard line length, be consistent in how you break up the lines.

### Return Values ###

Do not needlessly surround the return expression with parentheses.

### Variable and Array Initialization ###

Your choice of =, (), or {}.

### Preprocessor Directives ###

The hash mark that starts a preprocessor directive should always be at the beginning of the line.

### Class Format ###

Sections in public, protected and private order, with no indentation

### Constructor Initializer Lists ###

Constructor initializer lists can be all on one line or with subsequent lines indented four spaces.

### Namespace Formatting ###

The contents of namespaces are not indented.

### Horizontal Whitespace ###

Use of horizontal whitespace depends on location. Never put trailing whitespace at the end of a line.

### Trailing Whitespace ###

You shouldn't have trailing whitespace.

### Vertical Whitespace ###

Minimize use of vertical whitespace.

### Exceptions to the Rules ###

The coding conventions described above are mandatory. However, like all good rules, these sometimes have exceptions, which we discuss here.

### Existing Non-conformant Code ###

You may diverge from the rules when dealing with code that does not conform to this style guide.

### Parting Words ###
Use common sense and BE CONSISTENT.