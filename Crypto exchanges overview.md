[TOC]

# BitMEX #

## Overview ##

At overload streams are unreliable. Good API. Structures based on FIX.

## Demo platform ##

Yes

## Quotes streaming ##

Yes

## Execution stream ##

Yes, private

## Order change type ##

Order modify


# Bitflyer #

## Overview ##

* Cancel is asynchronous. Should check order status after cancel, or you can have cases when exchange reports about order fill after cancel. But when the order is canceled, exchange returns an empty list, instead of a list with the object with status = CANCELED. Moreover, the same situation will be when the order was filled.

* Delayed fill events in the stream, most likely at overloads. For instance, after the order canceling we got an execution report after 28 seconds.

* When receiving orders executions from the stream, have many duplicated records with either same id and payload or zero IDs. The only way to eliminate duplicates, check for owned order id.

## Demo platform ##

No

## Quotes streaming ##

Yes

## Execution stream ##

Yes, public

## Order modify ##

No. Cancel/replace only

## Customer support ##

Useless. "API works as it works"

# Bx.in.th #

## Overview ##

* Uncommon and inconvenient API. At order placing, for a buy order, you have to define amount in base currency, for sell in counter currency. Similar logic is getting an order, for the same instrument exchange returns amount either in the base currency, or counter one.

## Demo platform ##

No

## Quotes streaming ##

No

## Execution stream ##

No

## Order change type ##

Cancel/replace


# Huobi #

## Overview ##

Uncommon input amount currencies, similar "bx.in" (Is it common for the Chinese exchanges?). When the market buy order is placed, the order amount has to be defined in counter currency. For market sell, limit buy and limit sell, amount have to be defined at base currency. 

Also one drawback that information about orders minimal amount is posted on single HTML page and doesn't reachable by API.

## Demo platform ##

No

## Quotes streaming ##

Yes

## Execution stream ##

Yes, private

## Order modification ##

No

## Support ##

Good. Fast and professional.